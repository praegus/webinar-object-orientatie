# Welkom!
* Voor wie? 
    * Werk jij in een agile team waar men gebruikt maakt van Java?
    * Wil jij je developers beter begrijpen?
    * Wil jij unittesten kunnen begrijpen
    * Of misschien zelfs een testautomatiseringsframework in java kunnen ontwikkelen?
* Onderwerpen: 
    * Java basics
    * Methoden en klassen
    * Inheritence en Polymorphisme
* Let op: We gaan heel snel, en we behandelen veel 
    * https://gitlab.com/praegus/webinar-object-orientatie
    * https://www.youtube.com/user/praegus