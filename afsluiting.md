# Afsluiting
* Onderwerpen: 
    * Java basics
    * Methoden en klassen
    * Inheritence en Polymorphisme
* Naslagwerk: 
    * https://gitlab.com/praegus/webinar-object-orientatie
    * https://www.youtube.com/user/praegus
* Java voor testers cursus!
    * 17 april, 24 april, 8 mei en 15 mei. 
    * https://praegus.nl/academy/leergangen/java-voor-testers
    * 10% korting met deze code: "OOwebJava2019". 