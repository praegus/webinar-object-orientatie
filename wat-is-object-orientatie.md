# Wat is objectorientatie

* Het is een programmeerparadigma
* Het systeem wordt gerepresenteerd als een interactie tussen dingen; objecten. 
* Algemene doel: "overzicht houden"

Belangrijke eigenschappen: 
* Informatie verbergen
* Modulariteit
* Overerving
* Polymorfisme

https://nl.wikipedia.org/wiki/Objectgeorienteerd