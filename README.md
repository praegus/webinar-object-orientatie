# Webinar introductie objectorientatie

## Introductie
* [Welkom!](welkom.md) 
* [Wat is objectorientatie](wat-is-object-orientatie.md)
* [Wat is Java](wat-is-java.md)
* Java geinstalleerd / Java op pad ([link](https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK_Howto.html))
* Hello world 
* Integrated development environment (IDE) / Intellij 

## Class, methodes en variabelen 
* Methodes; return types
* ifthenelse, for
* constructors & this
* Scope van variabelen binnen class
    * method variabelen
    * class variabelen 
* Scope van methoden en variabelen buiten de class (access modifiers)
    * public 
    * private
    * waarom private variabelen? 
* Instantiering van klassen 
* Static variabelen en members 

## Inheritence en Polymorphisme 
* Inheritence
* Dierentuin
    * [Dierenhierarchie](hierarchie_dieren.png)
    * Abstract classes
    * Interfaces
  

* [Afsluiting](afsluiting.md)