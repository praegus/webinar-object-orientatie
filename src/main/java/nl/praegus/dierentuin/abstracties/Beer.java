package nl.praegus.dierentuin.abstracties;

public abstract class Beer extends Dier {
    private int aantalPoten = 4;

    public Beer(String naam) {
        super(naam);
    }

    public int getAantalPoten() {
        return aantalPoten;
    }

    public void setAantalPoten(int aantalPoten) {
        this.aantalPoten = aantalPoten;
    }
}
