package nl.praegus.dierentuin.abstracties;

public abstract class Dier {
    private String naam;
    private double temperatuur = 37.4;

    public Dier(String naam) {
        this.naam = naam;
    }

    public double getTemperatuur() {
        return temperatuur;
    }

    public void setTemperatuur(double temperatuur) {
        this.temperatuur = temperatuur;
    }

    public String getNaam() {
        return naam;
    }

    public String getInfo() {
        return "Dier " + getNaam() + " en heeft als temperatuur " + getTemperatuur() + " graden!";
    }
}
