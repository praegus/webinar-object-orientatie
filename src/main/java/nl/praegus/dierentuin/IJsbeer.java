package nl.praegus.dierentuin;

public class IJsbeer {
    private String naam;
    private int aantalPoten = 4;
    private double temperatuur = 37.4;

    public void setAantalPoten(int aantal) {
        aantalPoten = aantal;
    }

    public void setTemperatuur(double temp) {
        temperatuur = temp;
    }

    public String getInfo() {
        return "IJsbeer " + naam + " heeft " + aantalPoten + " poten en heeft als temperatuur " + temperatuur + " graden!";
    }

    // region constructors en this
    public IJsbeer() {}

    public IJsbeer(String naam) {
        this.naam = naam;
    }
    // endregion constructors en this

    //region ifthenelse
    public boolean isZiek() {
        if (temperatuur < 35 || aantalPoten!=4 || temperatuur > 40) {
            return true;
        } else {
            return false;
        }
    }
    // endregion ifthenelse

    // region loop
    private String[] dagelijksRitme = {"eet", "poept", "slaapt", "loopt"};

    public void printRitme() {
        for (int i = 0; i < dagelijksRitme.length; i++) {
            System.out.println(naam + " " + dagelijksRitme[i]);
        }
    }
    // endregion loop

    // region static
    private static String kleur = "wit";

    public static String getKleur() {
        return kleur;
    }

    // endregion static
}
