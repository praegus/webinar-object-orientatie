package nl.praegus.dierentuin.dieren;

import nl.praegus.dierentuin.abstracties.Beer;

public class Pandabeer extends Beer {

    public Pandabeer(String naam) {
        super(naam);
    }
}
