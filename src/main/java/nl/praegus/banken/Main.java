package nl.praegus.banken;

import nl.praegus.banken.persoon.NatuurlijkPersoon;
import nl.praegus.banken.persoon.NietNatuurlijkPersoon;
import nl.praegus.banken.rekening.Betaalrekening;
import nl.praegus.banken.rekening.Rekening;
import nl.praegus.banken.rekening.Spaarrekening;
import nl.praegus.banken.rekening.Transactie;

public class Main {

    public static void main(String[] args) {
        Spaarrekening rekeningPiet = getRekeningPiet();
        Rekening rekeningPraegus = getRekeningPraegus();
        Rekening rekeningCursist = getRekeningCursist();

        Bank spaarbank = new Bank();
        spaarbank.addRekening(rekeningPiet);
        spaarbank.addRekening(rekeningPraegus);
        spaarbank.addRekening(rekeningCursist);

        System.out.println();
        System.out.println("Bank spaarbank heeft " + spaarbank.getAantalRekeningen() + " rekeningen en en saldo van " + spaarbank.getSaldo());
        System.out.println();

        spaarbank.printRekeninghouders();
    }

    private static Rekening getRekeningCursist() {
        Rekening rekeningCursist = new Betaalrekening(new NatuurlijkPersoon("Jan", "van", "Voren", 1234564));
        rekeningCursist.voegTransactieToe(new Transactie("Hendrik Hendrikson", "6", 1034));
        System.out.println("Jan de cursist heeft een saldo van: " + rekeningCursist.getSaldo());
        return rekeningCursist;
    }

    private static Rekening getRekeningPraegus() {
        Rekening rekeningPraegus = new Betaalrekening(new NietNatuurlijkPersoon("Praegus B.V.", "54123124", "Leusden"));
        rekeningPraegus.voegTransactieToe(new Transactie("Sinterklaas", "4", 1034));
        rekeningPraegus.voegTransactieToe(new Transactie("Koningin Maxima", "5", 5330));
        System.out.println("Praegus heeft een saldo van: " + rekeningPraegus.getSaldo());
        return rekeningPraegus;
    }

    private static Spaarrekening getRekeningPiet() {
        Spaarrekening rekeningPiet = new Spaarrekening(new NatuurlijkPersoon("Piet", null, "Pietersen", 12345678));
        rekeningPiet.voegTransactieToe(new Transactie("Jan van Voren", "1", 10));
        rekeningPiet.voegTransactieToe(new Transactie("Praegus B.V.", "2", 30));
        rekeningPiet.voegTransactieToe(new Transactie("Nog iemand", "3", -23));
        System.out.println("Piet heeft een saldo van: " + rekeningPiet.getSaldo());
        rekeningPiet.keerRenteUit();
        System.out.println("Na renteuitkeer heeft piet een saldo van: " + rekeningPiet.getSaldo());
        return rekeningPiet;
    }
}
