package nl.praegus.banken.persoon;

public class NietNatuurlijkPersoon extends Persoon {
    private String naam;
    private String kvkNummer;
    private String stataireZetel;

    public NietNatuurlijkPersoon(String naam, String kvkNummer, String stataireZetel) {
        this.naam = naam;
        this.kvkNummer = kvkNummer;
        this.stataireZetel = stataireZetel;
    }

    @Override
    public String getNaam() {
        return naam;
    }

    public String getKvkNummer() {
        return kvkNummer;
    }

    public String getStataireZetel() {
        return stataireZetel;
    }
}
