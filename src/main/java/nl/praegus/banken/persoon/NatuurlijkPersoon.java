package nl.praegus.banken.persoon;

public class NatuurlijkPersoon extends Persoon {
    private String voornaam;
    private String tussenvoegsels;
    private String achternaam;
    private int bsn;

    public NatuurlijkPersoon(String voornaam, String tussenvoegsels, String achternaam, int bsn) {
        this.voornaam = voornaam;
        this.tussenvoegsels = tussenvoegsels;
        this.achternaam = achternaam;
        this.bsn = bsn;
    }

    @Override
    public String getNaam() {
        return voornaam + " " + tussenvoegsels + " " + achternaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getTussenvoegsels() {
        return tussenvoegsels;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public int getBsn() {
        return bsn;
    }
}
