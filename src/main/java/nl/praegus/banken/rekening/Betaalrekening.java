package nl.praegus.banken.rekening;

import nl.praegus.banken.persoon.Persoon;

public class Betaalrekening extends Rekening {

    public Betaalrekening(Persoon rekeninghouder) {
        super(rekeninghouder);
    }
}
