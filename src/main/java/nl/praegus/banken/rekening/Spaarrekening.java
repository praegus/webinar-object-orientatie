package nl.praegus.banken.rekening;

import nl.praegus.banken.persoon.Persoon;

public class Spaarrekening extends Rekening {
    private static double rente = 0.01;

    public Spaarrekening(Persoon rekeninghouder) {
        super(rekeninghouder);
    }

    public void keerRenteUit() {
        saldo *= (1 + rente);
    }
}
