package nl.praegus.banken.rekening;

import nl.praegus.banken.persoon.Persoon;

import java.util.ArrayList;
import java.util.List;

public abstract class Rekening {
    private Persoon rekeninghouder;
    protected double saldo;
    private List<Transactie> transacties;

    public Rekening(Persoon rekeninghouder) {
        this.rekeninghouder = rekeninghouder;
        this.saldo = 0.0;
        this.transacties = new ArrayList<>();
    }

    public void voegTransactieToe(Transactie transactie) {
        transacties.add(transactie);
        saldo += transactie.getBedrag();
    }

    public double getSaldo() {
        return saldo;
    }

    public Persoon getRekeninghouder() {
        return rekeninghouder;
    }

    public List<Transactie> getTransacties() {
        return transacties;
    }
}
