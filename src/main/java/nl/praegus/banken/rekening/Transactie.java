package nl.praegus.banken.rekening;

public class Transactie {
    private String vanPersoon;
    private String vanRekeningnummer;
    private double bedrag;

    public Transactie(String vanPersoon, String vanRekeningnummer, double bedrag) {
        this.vanPersoon = vanPersoon;
        this.vanRekeningnummer = vanRekeningnummer;
        this.bedrag = bedrag;
    }

    public String getVanPersoon() {
        return vanPersoon;
    }

    public String getVanRekeningnummer() {
        return vanRekeningnummer;
    }

    public double getBedrag() {
        return bedrag;
    }
}
