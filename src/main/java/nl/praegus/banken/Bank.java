package nl.praegus.banken;

import nl.praegus.banken.rekening.Rekening;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private List<Rekening> rekeningen = new ArrayList<>();

    public void addRekening(Rekening rekening) {
        rekeningen.add(rekening);
    }

    public int getAantalRekeningen() {
        return rekeningen.size();
    }

    public double getSaldo() {
        double saldo = 0;
        for (Rekening rekening : rekeningen) {
            saldo += rekening.getSaldo();
        }
        return saldo;
    }

    public void printRekeninghouders() {
        System.out.println("rekeninghouders zijn:");
        for (Rekening rekening : rekeningen) {
            System.out.println(rekening.getRekeninghouder().getNaam());
        }
    }
}
